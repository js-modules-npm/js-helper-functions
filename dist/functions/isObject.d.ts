import { IKeys } from '../types/types';
export declare function isObject<Keys = unknown>(object: unknown): object is IKeys<Keys>;
//# sourceMappingURL=isObject.d.ts.map