import { IKeys } from '../types/types';
export declare function get<T = unknown>(object: IKeys<unknown>, path: string, defaultValue?: T): T | undefined;
//# sourceMappingURL=get.d.ts.map