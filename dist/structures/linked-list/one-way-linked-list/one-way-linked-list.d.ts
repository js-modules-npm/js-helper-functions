import { IElementLinkedNode, LinkedNode } from '../linked-node/linked-node';
export declare class OneWayLinkedList<T = unknown> {
    #private;
    constructor(iterable: Iterable<T>);
    get length(): number;
    get head(): IElementLinkedNode<T>;
    get tail(): IElementLinkedNode<T>;
    append(value: T): LinkedNode<T>;
    prepend(value: T): LinkedNode<T>;
    add(value: T, index: number): IElementLinkedNode<T>;
    find(value: T): IElementLinkedNode;
    findIndex(value: T): number;
    findByIndex(number: number): IElementLinkedNode;
    contains(value: T): boolean;
    remove(value: T): boolean;
    removeLast(): boolean;
    removeFirst(): boolean;
    removeByIndex(value: number): boolean;
    convertToArray(): T[];
}
//# sourceMappingURL=one-way-linked-list.d.ts.map