export type IElementLinkedNode<T = unknown> = LinkedNode<T> | null;
export declare class LinkedNode<T = unknown> {
    value: T;
    next: IElementLinkedNode<T>;
    constructor(value: T, next?: IElementLinkedNode<T>);
}
//# sourceMappingURL=linked-node.d.ts.map