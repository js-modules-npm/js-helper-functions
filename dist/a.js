(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("js-helper-functions", [], factory);
	else if(typeof exports === 'object')
		exports["js-helper-functions"] = factory();
	else
		root["js-helper-functions"] = factory();
})(this, () => {
return /******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/functions/deepClone.ts":
/*!************************************!*\
  !*** ./src/functions/deepClone.ts ***!
  \************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.deepClone = void 0;
const isObject_1 = __webpack_require__(/*! ./isObject */ "./src/functions/isObject.ts");
function deepClone(object) {
    if (typeof object === 'object') {
        const data = (0, isObject_1.isObject)(object) ? {} : [];
        for (const key in object) {
            data[key] = typeof object[key] === 'object'
                ? deepClone(object[key])
                : object[key];
        }
        return data;
    }
    return object;
}
exports.deepClone = deepClone;


/***/ }),

/***/ "./src/functions/isObject.ts":
/*!***********************************!*\
  !*** ./src/functions/isObject.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.isObject = void 0;
function isObject(object) {
    return !!(typeof object === 'object' && object && !Array.isArray(object));
}
exports.isObject = isObject;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
/*!******************!*\
  !*** ./src/a.ts ***!
  \******************/

Object.defineProperty(exports, "__esModule", ({ value: true }));
const deepClone_1 = __webpack_require__(/*! ./functions/deepClone */ "./src/functions/deepClone.ts");
class Base {
    base = 1;
}
class Second extends Base {
    second = 2;
}
const obj = {
    a: 1,
    objInside: {
        a: 1
    },
    log() {
        console.log(this.a);
    },
    data: new Second(),
    arr: [
        1,
        2,
        [
            [
                {
                    a: 1,
                }
            ]
        ]
    ]
};
const data = (0, deepClone_1.deepClone)(obj);
data.a = 2;
data.objInside.a = 2;
data.data.base = 2;
data.arr[2][0][0].a = 2;
console.log('obj', obj);
console.log('data', data);
obj.log();
data.log();

})();

/******/ 	return __webpack_exports__;
/******/ })()
;
});
//# sourceMappingURL=a.js.map