import { deepClone } from './functions/deepClone';


class Base {
	public base = 1;
}

class Second extends Base {
	public second = 2;
}

const obj = {
	a: 1,
	objInside: {
		a: 1
	},
	log() {
		console.log(this.a);
	},
	data: new Second(),
	arr: [
		1,
		2,
		[
			[
				{
					a: 1,
				}
			]
		]
	]
};

const data = deepClone(obj);

data.a = 2;
data.objInside.a = 2;
data.data.base = 2;
(data as any).arr[2][0][0].a = 2;

console.log('obj', obj);
console.log('data', data);


obj.log();
data.log();
