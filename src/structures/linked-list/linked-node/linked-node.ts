export type IElementLinkedNode<T = unknown> = LinkedNode<T> | null;

export class LinkedNode<T = unknown> {
	public value: T;
	public next: IElementLinkedNode<T>;

	constructor(
		value: T,
		next: IElementLinkedNode<T> = null,
	) {
		this.value = value;
		this.next = next;
	}
}
