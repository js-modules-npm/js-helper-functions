import {
	IElementLinkedNode,
	LinkedNode
} from '../linked-node/linked-node';

interface IReturnFindValues<T = unknown> {
    previousNode: IElementLinkedNode<T>,
    currentNode: IElementLinkedNode<T>,
    nextNode: IElementLinkedNode<T>,
    foundIndex: number,
}

interface ISearchProps<T = unknown> {
    value?: T,
    index?: number,
}

export class OneWayLinkedList<T = unknown> {
	#head: IElementLinkedNode<T> = null;
	#tail: IElementLinkedNode<T> = null;
	#length = 0;

	public constructor(iterable: Iterable<T>) {
		this.#createLinkedListFromIterable(iterable);
	}

	public get length(): number {
		return this.#length;
	}

	public get head(): IElementLinkedNode<T> {
		return this.#head;
	}

	public get tail(): IElementLinkedNode<T> {
		return this.#tail;
	}

	public append(value: T): LinkedNode<T> {
		const newNode: LinkedNode<T> = new LinkedNode(value);

		if (this.#head && this.tail) {
			const previousTail = this.tail;
			previousTail.next = this.#tail = newNode;
		} else {
			this.#head = this.#tail = newNode;
		}

		this.#length++;
		return newNode;
	}

	public prepend(value: T): LinkedNode<T> {
		const newNode: LinkedNode<T> = new LinkedNode(value);

		if (this.#head && this.#tail) {
			const previousHead = this.#head;
			this.#head = newNode;
			this.#head.next = previousHead;
		} else {
			this.#head = this.#tail = newNode;
		}

		return newNode;
	}

	public add(value: T, index: number): IElementLinkedNode<T> {
		const {currentNode, nextNode} = this.#findNeighborsByIndex(index);
		const newNode: LinkedNode<T> = new LinkedNode(value);

		if (currentNode) {
			if (nextNode) {
				currentNode.next = newNode;
				newNode.next = nextNode;
			} else {
				currentNode.next = this.#tail = newNode;
			}

			this.#length++;
			return newNode;
		} else {
			if (index === -1) {
				if (this.#head && this.#tail) {
					const previousHead = this.#head;
					this.#head = newNode;
					newNode.next = previousHead;
				}

				if (!this.#head && !this.#tail) {
					this.#head = this.#tail = newNode;
					return newNode;
				}

				this.#length++;
				return newNode;
			}
		}

		return null;
	}

	public find(value: T): IElementLinkedNode {
		const {currentNode} = this.#findNeighborsByValue(value);

		if (currentNode) {
			return currentNode;
		}

		return null;
	}

	public findIndex(value: T): number {
		return this.#findNeighborsByValue(value).foundIndex;
	}

	public findByIndex(number: number): IElementLinkedNode {
		number = +number;

		if (Object.is(number, NaN)) {
			console.error('argument must be number');
			return null;
		}

		return this.#findNeighborsByIndex(number).currentNode;
	}

	public contains(value: T): boolean{
		return !!this.find(value);
	}

	public remove(value: T): boolean {
		const {previousNode, currentNode, nextNode} = this.#findNeighborsByValue(value);

		if (currentNode) {
			if (!previousNode && nextNode) {
				this.#head = nextNode;
			}

			if (!previousNode && !nextNode) {
				this.#head = this.#tail = null;
			}

			if (previousNode && !nextNode) {
				previousNode.next = null;
				this.#tail = previousNode;
			}

			if (previousNode && nextNode) {
				previousNode.next = nextNode;
			}

			this.#length--;
			return true;
		}

		return false;
	}

	public removeLast(): boolean {
		if (this.#tail) {
			const {previousNode, currentNode} = this.#findNeighborsByValue(this.#tail.value);

			if (currentNode) {
				if (previousNode) {
					previousNode.next = null;
					this.#tail = previousNode;
				} else {
					this.#head = this.#tail = null;
				}

				this.#length--;
				return true;
			}
		}

		return false;
	}

	public removeFirst(): boolean {
		if (this.#head) {
			const {currentNode, nextNode} = this.#findNeighborsByValue(this.#head.value);

			if (currentNode) {
				if (nextNode) {
					this.#head = nextNode;
				} else {
					this.#head = this.#tail = null;
				}

				this.#length--;
				return true;
			}
		}

		return false;
	}

	public removeByIndex(value: number): boolean {
		const {previousNode, currentNode, nextNode} = this.#findNeighborsByIndex(value);

		if (currentNode) {
			if (!previousNode && !nextNode) {
				this.#head = this.#tail = null;
			}

			if (!previousNode && nextNode) {
				this.#head = nextNode;
			}

			if (previousNode && !nextNode) {
				previousNode.next = null;
				this.#tail = previousNode;
			}

			if (previousNode && nextNode) {
				previousNode.next = nextNode;
			}

			this.#length--;
			return true;
		}

		return false;
	}

	public convertToArray(): T[] {
		const array: T[] = [];
		let currentNode: IElementLinkedNode<T> = this.#head;

		while (currentNode) {
			array.push(currentNode.value);
			currentNode = currentNode.next;
		}

		return array;
	}

	#findNeighborsByValue(value: T): IReturnFindValues<T> {
		return this.#findNeighborsByProps({value});
	}

	#findNeighborsByIndex(index: number): IReturnFindValues<T> {
		return this.#findNeighborsByProps({index});
	}

	#findNeighborsByProps({index: searchIndex, value}: ISearchProps<T>): IReturnFindValues<T> {
		const nodes: IReturnFindValues<T> = {
			previousNode: null,
			currentNode: this.#head,
			nextNode: null,
			foundIndex: -1,
		};

		let index = 0;

		while (nodes.currentNode) {
			const nextNode: IElementLinkedNode<T> = nodes.currentNode.next;

			const isSearchIndexRightBoolean: boolean = typeof searchIndex === 'number'
                && !Object.is(searchIndex, NaN)
                && searchIndex === index;

			if (
				isSearchIndexRightBoolean && Object.is(nodes.currentNode.value, value)
                || isSearchIndexRightBoolean
                || Object.is(nodes.currentNode.value, value)
			) {
				nodes.nextNode = nextNode;
				nodes.foundIndex = index;
				return nodes;
			}

			nodes.previousNode = nodes.currentNode;
			nodes.currentNode = nextNode;
			index++;
		}

		return {
			previousNode: null,
			currentNode: null,
			nextNode: null,
			foundIndex: -1,
		};
	}

	#createLinkedListFromIterable(iterable: Iterable<T>): void {
		if (Symbol.iterator in iterable) {
			const iterator: Iterator<T> = iterable[Symbol.iterator]();
			let currentValue: IteratorResult<T> | undefined;

			for (;;) {
				currentValue = iterator.next();

				if(currentValue.done) {
					break;
				}

				this.append(currentValue.value);
			}
		}
	}
}
