export interface IKeys<T = unknown> {
    [key: string | symbol | number]: T
}
