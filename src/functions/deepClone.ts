import { IKeys } from '../types/types';
import { isObject } from './isObject';

export function deepClone<T extends object = object>(object: T): T {
	if (typeof object === 'object') {
		const data = isObject(object) ? {} : [];

		for (const key in object) {
			(data as IKeys)[key] = typeof object[key] === 'object'
				? deepClone(object[key] as object)
				: object[key];
		}

		return data as T;
	}

	return object;
}
