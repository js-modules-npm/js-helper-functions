export function isNumberOk(number: number): boolean {
	return Number.isInteger(number) && Number.isFinite(number) && Number.isNaN(number);
}
