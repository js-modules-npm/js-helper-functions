import { IKeys } from '../types/types';

export function isObject<Keys = unknown>(object: unknown): object is IKeys<Keys> {
	return !!(typeof object === 'object' && object && !Array.isArray(object));
}
