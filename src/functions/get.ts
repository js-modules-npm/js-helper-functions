import { IKeys } from '../types/types';
import { isObject } from './isObject';

export function get<T = unknown>(object: IKeys<unknown>, path: string, defaultValue?: T): T | undefined {
	const splittedPaths = path.split('.');
	let timelyObject: IKeys<unknown> = object;

	for (const [index, key] of splittedPaths.entries()) {
		if (key in timelyObject) {
			if (index === splittedPaths.length - 1) return timelyObject[key] as T;
			const data = timelyObject[key];
			if (isObject(data)) timelyObject = data;
			continue;
		}

		return defaultValue;
	}
}
