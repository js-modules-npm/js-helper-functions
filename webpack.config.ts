import path from 'path';
import fs from 'fs';
import webpack from 'webpack';

const getConfig = (): webpack.Configuration => {
	return {
		mode: 'development',
		target: 'web',
		entry: getEntryFiles,
		devtool: 'source-map',
		output: {
			publicPath: 'auto',
			filename: '[name].js',
			chunkFilename: '[name].js',
			path: path.resolve(__dirname, 'dist'),
			charset: true,
			clean: true,
			chunkLoadTimeout: 20000,
			asyncChunks: true,
			iife: true,
			scriptType: 'text/javascript',
			library: {
				name: 'js-helper-functions',
				type: 'umd',
			},
			umdNamedDefine: true,
			globalObject: 'this',
		},
		resolve: {
			extensions: [
				'.jsx',
				'.tsx',
				'.js',
				'.ts',
			],
			modules: ['node_modules'],
		},
		module: {
			rules: [
				{
					test: /\.tsx?$/,
					use: [
						{
							loader: 'ts-loader',
						},
					],
					exclude: /node_modules/,
				},
			],
		},
		plugins: [
		],
		optimization: {
			splitChunks: {
				chunks: 'async',
				minSize: 20000,
				minRemainingSize: 0,
				minChunks: 1,
				maxAsyncRequests: 30,
				maxInitialRequests: 30,
				enforceSizeThreshold: 50000,
				cacheGroups: {
					defaultVendors: {
						test: /[\\/]node_modules[\\/]/,
						priority: -10,
						reuseExistingChunk: true,
					},
					default: {
						minChunks: 2,
						priority: -20,
						reuseExistingChunk: true,
					},
				},
			},
		}
	};
};

export default [getConfig];

function getEntryFiles(): webpack.EntryObject {
	const modulesPath = path.resolve(__dirname, 'src');
	const result: webpack.EntryObject = {};

	fs.readdirSync(modulesPath).forEach(item => {
		if (item.includes('.ts')) {
			const key = item.replace(/.ts$/, '');
			result[key] = modulesPath + '/' + item;
		}
	});

	return result;
}
